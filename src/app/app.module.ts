import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'

import { AppComponent } from './app.component'
import {NbThemeModule } from '@nebular/theme'
import { NebularModule } from './nebular/nebular.module'
import { NgxsModule } from '@ngxs/store'
import { NgxsRouterPluginModule } from '@ngxs/router-plugin'

// ngxs redux devtools
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin'
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin'
import { AppState } from './shared/app.state';
import { RouterState } from './shared/router.state';

// Routes
export const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'books'
  },
  {
    path: 'books',
    loadChildren: './books/books.module#BooksModule'
  }
  
]
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
     NebularModule,
    NbThemeModule.forRoot({name: 'default'}),
    NgxsModule.forRoot([
      AppState
    ]),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsLoggerPluginModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
