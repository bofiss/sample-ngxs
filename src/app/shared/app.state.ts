import { State, StateContext, Action } from "@ngxs/store";

import { SetUsername } from "./app.actions";

export interface AppStateModel {
    username: string,
    status: 'active' | 'inactive'
}


const defaults: AppStateModel = {
    username: 'Belle Belle',
    status: 'active'
}

@State<AppStateModel>({
    name: 'app',
    defaults
})

export class AppState {

  @Action(SetUsername)
  setUsername({patchState, getState}: StateContext<AppStateModel>, {payload}: SetUsername){ 
      const currentUser = getState().username
      patchState({username: payload})
  }
 
}