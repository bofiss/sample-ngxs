export class SetUsername {
    static readonly type = '[App] Set Username'
    constructor(public payload: string){}
}
export class Navigate {
    static readonly type = '[Router] Navigate'
    constructor(public payload: string){}
}