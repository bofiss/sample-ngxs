import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'
import {NbSidebarModule, NbLayoutModule, NbSidebarService, NbCardModule, NbSearchModule, NbMenuModule, } from '@nebular/theme'
import { NbMenuInternalService, NbMenuService } from '@nebular/theme/components/menu/menu.service';

@NgModule({
  imports: [
    CommonModule,
    NbLayoutModule,
    NbSidebarModule,
    NbCardModule,
    NbSearchModule,
    NbMenuModule, 

  ],
  exports: [NbLayoutModule, NbSidebarModule, NbCardModule, NbSearchModule, NbMenuModule],
  providers: [NbSidebarService, NbMenuInternalService, NbMenuService],
  declarations: []
})
export class NebularModule { }
