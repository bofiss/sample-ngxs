import { BookListComponent } from './book-list/book-list.component';
import { BookDetailsComponent } from './book-details/book-details.component';


export const components: any[] = [
    BookDetailsComponent,
    BookListComponent
]

export { BookDetailsComponent } from  './book-details/book-details.component';
export { BookListComponent } from './book-list/book-list.component';




