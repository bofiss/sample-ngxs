import { BookPageComponent } from './book-page/book-page.component';
import { BookSelectedPageComponent } from './book-selected-page/book-selected-page.component';


export const containers: any[] = [
    BookPageComponent,
    BookSelectedPageComponent
]

export { BookPageComponent } from './book-page/book-page.component';
export { BookSelectedPageComponent } from './book-selected-page/book-selected-page.component';