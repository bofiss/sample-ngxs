import { Component, OnInit } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Book } from '../../models/book';
import { Store, Actions, ofAction, ofActionDispatched, ofActionSuccessful } from '@ngxs/store';
import * as fromRouter from '@ngxs/router-plugin'
import { RouterNavigation } from '@ngxs/router-plugin';
import { BookDataService } from '../../services';

@Component({
  selector: 'app-book-selected-page',
  templateUrl: './book-selected-page.component.html',
  styleUrls: ['./book-selected-page.component.css']
})
export class BookSelectedPageComponent implements OnInit {
  id:number;
  rating: number;
  book$: Observable<Book>;
  constructor(private sb: BookDataService, private actions$: Actions, private store: Store, private restangular: Restangular, private route: ActivatedRoute) {
   
      
  }

  ngOnInit() {
    this.actions$.pipe(
      ofAction(RouterNavigation)
    ).subscribe(payload => { 
      console.log(payload)
      this.sb.setId(payload.routerState.root.firstChild.firstChild.params.id)})
    console.log(this.sb.id)
      this.book$ = this.store.select(state =>state.books.booksEntities[this.sb.id] )
      this.book$.subscribe(book => this.rating = parseFloat(book.average_rating))
    } 

 
  

}
