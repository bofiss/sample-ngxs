import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookSelectedPageComponent } from './book-selected-page.component';

describe('BookSelectedPageComponent', () => {
  let component: BookSelectedPageComponent;
  let fixture: ComponentFixture<BookSelectedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookSelectedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookSelectedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
