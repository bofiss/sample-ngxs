import { Component, OnInit } from '@angular/core';
import { Restangular } from 'ngx-restangular';
import { of, Observable, } from "rxjs";
import { Book } from '../../models/book';
import { Select, Store } from '@ngxs/store';
import { BookState } from '../../store/book.state';
import { LoadBooks } from '../../store/actions/book.actions';

@Component({
  selector: 'app-book-page',
  templateUrl: './book-page.component.html',
  styleUrls: ['./book-page.component.css']
})
export class BookPageComponent implements OnInit {

  books$: Observable<any>
  constructor(private restangular: Restangular, private store: Store) { }

  ngOnInit() {
   this.store.dispatch(new LoadBooks())
   this.books$ = this.store.select(state => Object.keys(state.books.booksEntities)
   .map(id => state.books.booksEntities[parseInt(id, 10)]))
  }


}
