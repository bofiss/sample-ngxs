import { Book } from "../models/book";
import { Restangular } from "ngx-restangular";
import { State, Selector, StateContext, Action,  } from "@ngxs/store";
import { LoadBooks } from "./actions/book.actions";

export interface BookStateModel  {
    booksEntities:  {[id: number]: Book}
}


const defaults: BookStateModel = {
    booksEntities: {}
}

@State<BookStateModel>({
    name: 'books',
    defaults
})
export class BookState {
    constructor(private restangular: Restangular){}

    @Action(LoadBooks)
    async loadBooks(ctx: StateContext<BookStateModel>, action: LoadBooks){
        const state = ctx.getState()
        const result = await this.restangular.all('books').getList().toPromise()
        const booksEntities = result.reduce((entities: {[id: number]: Book}, book) => {
            return { 
                ...entities,
                [book.id]: book
            };
        },{
            ...state.booksEntities
        })

        ctx.setState({
            ...state,
            booksEntities
            
        })
    }
    @Selector()
    static getBook(state: Book[] ){
  
    }
}
