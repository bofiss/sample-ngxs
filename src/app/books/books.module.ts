import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RestangularModule, Restangular } from 'ngx-restangular'
import {NebularModule} from '../nebular/nebular.module'
import { BarRatingModule } from "ngx-bar-rating";
import { NgxsModule } from '@ngxs/store';
import { NgxsRouterPluginModule} from '@ngxs/router-plugin'
// Setting default restangular configuration
 export function RestangularConfigFactory (RestangularProvider) {
   RestangularProvider.setBaseUrl('http://localhost:3000/api');
   RestangularProvider.setDefaultRequestParams(['get', 'getList'], {_start: 20, _limit: 12})
 }

// Components
import * as fromComponents from './components'

// Containers
import * as fromContainers from './containers'

// Services
import * as fromServices from './services'
import { BookState } from './store/book.state';

// routes 
export const ROUTES: Routes = [
  {
    path: '',
    component: fromContainers.BookPageComponent
  },
  {
    path: ':id',
    component: fromContainers.BookSelectedPageComponent
  }
]
@NgModule({
  imports: [
    CommonModule,
    NebularModule,
    BarRatingModule,
    RouterModule.forChild(ROUTES),
    RestangularModule.forRoot(RestangularConfigFactory),
    NgxsModule.forFeature([BookState])

  ],
  providers: [...fromServices.services,],
  declarations: [...fromComponents.components, ...fromContainers.containers],
  exports: [...fromComponents.components, ...fromContainers.containers]
})
export class BooksModule { }
